#!/usr/bin/python3
##############################################################################
# project_siterip - Downloads all vids in bulk
#
# Copyright (C) 2023 mrfuk
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

import requests, re
from bs4 import BeautifulSoup
import os
import shutil
import json
import time
import subprocess
import shlex
from zipfile import ZipFile
from pySmartDL import SmartDL
from pathlib import Path
from tqdm import tqdm


def getlibs():
    print("Initializing Resources")
    if not os.path.exists("libs"):
        os.mkdir("libs")
    if not os.path.exists("libs/youtube-dl.exe"):
        print("Downloading youtube-dl")
        url = "https://youtube-dl.org/downloads/latest/youtube-dl.exe"
        dl = requests.get(url, stream=True, allow_redirects=True)
        with open("libs/" + url.split("/")[-1], "wb") as fout:
            for chunk in dl.iter_content(chunk_size=1024):
                fout.write(chunk)

    if not os.path.exists("libs/aria2c.exe"):
        print("Downloading aria2c")
        url = "https://github.com/aria2/aria2/releases/download/release-1.36.0/aria2-1.36.0-win-64bit-build1.zip"
        dl = requests.get(url, stream=True, allow_redirects=True)
        with open("libs/" + url.split("/")[-1], "wb") as fout:
            for chunk in dl.iter_content(chunk_size=1024):
                fout.write(chunk)
        with ZipFile("libs/aria2-1.36.0-win-64bit-build1.zip") as dl:
            dl.extract("aria2-1.36.0-win-64bit-build1/aria2c.exe", path="libs")
        dl.close()
        os.rename("libs/aria2-1.36.0-win-64bit-build1/aria2c.exe", "libs/aria2c.exe")
        os.rmdir("libs/aria2-1.36.0-win-64bit-build1")
        os.remove("libs/aria2-1.36.0-win-64bit-build1.zip")

    if not os.path.exists("libs/ffprobe.exe"):
        print("Downloading ffprobe")
        url = "https://github.com/ffbinaries/ffbinaries-prebuilt/releases/download/v4.4.1/ffprobe-4.4.1-win-64.zip"
        dl = requests.get(url, stream=True, allow_redirects=True)
        with open("libs/" + url.split("/")[-1], "wb") as fout:
            for chunk in dl.iter_content(chunk_size=1024):
                fout.write(chunk)
        with ZipFile("libs/ffprobe-4.4.1-win-64.zip") as dl:
            dl.extract("ffprobe.exe", path="libs")
        dl.close()
        os.remove("libs/ffprobe-4.4.1-win-64.zip")


getlibs()


def parseCookieFile(cookiefile):
    cookies = {}
    with open(cookiefile, "r") as fp:
        for line in fp:
            if not re.match(r"^\#", line):
                lineFields = re.findall(r"[^\s]+", line)
                try:
                    cookies[lineFields[5]] = lineFields[6]
                except Exception as e:
                    pass
    return cookies


print(
    "1 - Analonly  2 - Trueanal  3 - Allanal  4 - Nympho  5 - Swallowed  6 - Bangbros  7 - Newsensations  8 - Pervcity  9 - Evilangel"
)
while True:
    choice = input("Enter Choice: ")
    print("")
    if choice == "1":
        site = "analonly"
        break
    elif choice == "2":
        site = "trueanal"
        break
    elif choice == "3":
        site = "allanal"
        break
    elif choice == "4":
        site = "nympho"
        break
    elif choice == "5":
        site = "swallowed"
        break
    elif choice == "6":
        site = "bangbros"
        break
    elif choice == "7":
        print("don't have subscription")
        exit(1)
        # site = "newsensations"
        # break
    elif choice == "8":
        site = "pervcity"
        break
    elif choice == "9":
        print("don't have subscription")
        exit(1)
        # site = "evilangel"
        # break
    else:
        print("Invalid Choice!, Try Again")


print("method")
print("1 - by pages  2 - by model")
while True:
    choice = input("Enter Choice: ")
    print("")
    if choice == "1":
        method = "pages"
        break
    elif choice == "2":
        method = "models"
        model = input("Enter Model Name: ")
        break
    else:
        print("Invalid Choice!, Try Again")


def getquality(site):
    print("Select Quality")
    if (
        site == "analonly"
        or site == "trueanal"
        or site == "allanal"
        or site == "nympho"
        or site == "swallowed"
    ):
        print("1 - 1080p [High Bitrate]   2 - 1080p   3 - 720p   4 - 360p")
        while True:
            choice = input("Enter Choice: ")
            print("")
            if choice == "1":
                quality = "orig"
                break
            elif choice == "2":
                quality = "hq"
                break
            elif choice == "3":
                quality = "stream"
                break
            elif choice == "4":
                quality = "mobile"
                break
            else:
                print("Invalid Choice!, Try Again")

    elif site == "bangbros":
        print("1 - 2160p   2 - 1080p   3 - 720p   4 - 480p")
        while True:
            choice = input("Enter Choice: ")
            print("")
            if choice == "1":
                quality = "2160p"
                break
            elif choice == "2":
                quality = "1080p"
                break
            elif choice == "3":
                quality = "720p"
                break
            elif choice == "4":
                quality = "480p"
                break
            else:
                print("Invalid Choice!, Try Again")

    elif site == "pervcity":
        print("1 - 2160p   2 - 1080p   3 - 720p   4 - 480p")
        while True:
            choice = input("Enter Choice: ")
            print("")
            if choice == "1":
                quality = "2160p"
                break
            elif choice == "2":
                quality = "mp412000"
                break
            elif choice == "3":
                quality = "mp48000"
                break
            elif choice == "4":
                quality = "mp42000"
                break
            else:
                print("Invalid Choice!, Try Again")

    return quality


quality = getquality(site)


def getcookies(site):
    if not os.path.exists("cookies"):
        os.mkdir("cookies")
    cfile = f"cookies/" + site + ".txt"
    if not Path.exists(Path(cfile)):
        print("cookies file missing:", site + ".txt")
        exit(1)
    return parseCookieFile(cfile), cfile


cookies, cfile = getcookies(site)
headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"}

print("Options")
print("1 - Download  2 - Print  3 - Print in txt file")
while True:
    choice = input("Enter Choice: ")
    print("")
    if choice == "1":
        want = "download"
        break
    elif choice == "2":
        want = "print"
        break
    elif choice == "3":
        want = "fprint"
        if not os.path.isdir(f"links/"):
            try:
                os.makedirs(f"links/")
            except OSError as error:
                print(error)
        f = open(f"links/" + site + " links.txt", "w+")
        break
    else:
        print("Invalid Choice!, Try Again")

if want == "download":
    print("Download Location")
    print("1 - Default(downloads in root dir)  2 - Custom dir")
    while True:
        choice = input("Enter Choice: ")
        print("")
        if choice == "1":
            dloc = f"downloads/"
            break
        elif choice == "2":
            dloc = input("Enter Download Location: ")
            break
        else:
            print("Invalid Choice!, Try Again")

    print("Choose Downloader")
    if site == "pervcity":
        print("1 - Aria(best)  2 - tqdm(ok)")
    else:
        print("1 - Aria(best)  2 - tqdm(ok)  3 - pySmartDL(good)")
    while True:
        choice = input("Enter Choice: ")
        print("")
        if choice == "1":
            downloader = "aria"
            break
        elif choice == "2":
            downloader = "tqdm"
            break
        elif choice == "3" and site != "pervcity":
            downloader = "pySmartDL"
            break
        else:
            print("Invalid Choice!, Try Again", "\n")


def get_filename(url):
    if (
        site == "analonly"
        or site == "trueanal"
        or site == "allanal"
        or site == "nympho"
        or site == "swallowed"
    ):
        fname = url.split("?")[0]
        fname = fname.split("/")[10]
    elif site == "bangbros":
        fname = url.split("?")[0]
        fname = fname.split("/")[7]
    elif site == "pervcity":
        fname = url.split("/")[-1]
    else:
        r = requests.head(url, cookies=cookies, allow_redirects=True)
        try:
            cd = r.headers["content-disposition"]
        except KeyError:
            return None
        if not cd:
            return None
        f = re.findall("filename=(.+)", cd)
        if len(f) == 0:
            return None
        fname = f[0]
        if (fname[0] == fname[-1]) and fname.startswith(("'", '"')):
            fname = fname[1:-1]
    return fname


def check_video(fname):
    cmd = "libs/ffprobe.exe " + fname
    cmd = shlex.split(cmd)
    a = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    return a.returncode


def download(url):
    global downloader
    global dloc
    fname = get_filename(url)
    fname = dloc + site.capitalize() + "/" + fname
    fpath = Path(fname)
    dname = os.path.dirname(fname)
    if not os.path.isdir(dname):
        try:
            os.makedirs(os.path.expanduser(dname))
        except OSError as error:
            print(error)
    if Path.exists(fpath):
        if site == "pervcity":
            if str(check_video(fname)) == "1":
                print("Corrupt file, Downloading Again")
            elif str(check_video(fname)) == "0":
                print("File " + os.path.basename(fname) + " exists: Skipping.\n")
                return 1
            else:
                print("Invalid Data")
                return 1
        else:
            r = requests.head(url, cookies=cookies, allow_redirects=True)
            length = r.headers["Content-Length"]
            fsize = Path(fname).stat().st_size
            if int(length) == fsize:
                print("File " + os.path.basename(fname) + " exists: Skipping.\n")
                return 1

    print("Downloading ", get_filename(url))
    if downloader == "aria":
        cmd = (
            "libs/youtube-dl.exe --no-check-certificate --cookies "
            + cfile
            + " -o "
            + fname
            + " "
            + url
            + ' --external-downloader libs/aria2c.exe --external-downloader-args "-x 16 -s 16 -k 1M --file-allocation=none --check-certificate=false"'
        )
        cmd = shlex.split(cmd)
        subprocess.run(cmd)

    elif downloader == "pySmartDL":
        temp = f"temp/"
        if not os.path.isdir(temp):
            try:
                os.makedirs(os.path.expanduser(temp))
            except OSError as error:
                print(error)
        obj = SmartDL(url, dest=temp)
        obj.start()
        ptime = obj.get_eta()
        if int(ptime) > 60:
            print("ETA:", round(int(ptime) / 60, 2), "min")
        else:
            print("ETA:", int(ptime), "sec")
        shutil.move(temp + os.path.basename(fname), dname)

    elif downloader == "tqdm":
        chunk_size = 1024
        dl = requests.get(url, cookies=cookies, stream=True, allow_redirects=True)
        if site == "pervcity":
            with open(fname, "wb") as fout:
                with tqdm(
                    unit="B",
                    unit_scale=True,
                    unit_divisor=1024,
                    miniters=1,
                    desc=os.path.basename(fname),
                ) as pbar:
                    for chunk in dl.iter_content(chunk_size=chunk_size):
                        fout.write(chunk)
                        pbar.update(len(chunk))
        else:
            with open(fname, "wb") as fout:
                with tqdm(
                    unit="B",
                    unit_scale=True,
                    unit_divisor=1024,
                    miniters=1,
                    desc=os.path.basename(fname),
                    total=int(dl.headers.get("content-length")),
                ) as pbar:
                    for chunk in dl.iter_content(chunk_size=chunk_size):
                        fout.write(chunk)
                        pbar.update(len(chunk))


def downloadlink(url):
    a = []
    global f
    global quality
    if (
        site == "analonly"
        or site == "trueanal"
        or site == "allanal"
        or site == "nympho"
        or site == "swallowed"
    ):
        url = "https://members." + site + ".com" + url
        try:
            r = requests.get(url, cookies=cookies, headers=headers)
        except:
            print("Connection Error, Skipping Scene")
            return 1
        soup = BeautifulSoup(r.content, "html5lib")
        script = soup.find("script", {"type": "application/json"}).contents[0]
        data = json.loads(script.text)
        url = data["props"]["pageProps"]["content"]["videos"][quality]["url"]
        if want == "download":
            download(url)
        elif want == "print":
            print(url, "")
        elif want == "fprint":
            print(get_filename(url), " copied!")
            print(url, "", file=f)

    elif site == "bangbros":
        url = "https://members.bluepillmen.com" + url
        try:
            r = requests.get(url, cookies=cookies, headers=headers)
        except:
            print("Connection Error, Skipping Scene")
            return 1
        soup = BeautifulSoup(r.content, "html5lib")
        for link in soup.find_all("a", href=True):
            if quality + ".mp4" in link["href"]:
                a = 1
                if want == "download":
                    download(link["href"])
                elif want == "print":
                    print(link["href"], "\n")
                elif want == "fprint":
                    print(get_filename(link["href"]), " copied!")
                    print(link["href"], file=f)
                    f.close()

            elif quality == "2160p" and "1080p.mp4" in link["href"] and a != 1:
                a = 1
                if want == "download":
                    download(link["href"])
                elif want == "print":
                    print(link["href"], "\n")
                elif want == "fprint":
                    print(get_filename(link["href"]), " copied!")
                    print(link["href"], file=f)
                    f.close()

            elif quality == "1080p" and "720p.mp4" in link["href"] and a != 1:
                a = 1
                if want == "download":
                    download(link["href"])
                elif want == "print":
                    print(link["href"], "\n")
                elif want == "fprint":
                    print(get_filename(link["href"]), " copied!")
                    print(link["href"], file=f)
                    f.close()

            elif quality == "720p" and "480p.mp4" in link["href"] and a != 1:
                if want == "download":
                    download(link["href"])
                elif want == "print":
                    print(link["href"], "\n")
                elif want == "fprint":
                    print(get_filename(link["href"]), " copied!")
                    print(link["href"], file=f)
                    f.close()

    elif site == "newsensations":
        url = "https://newsensations.com/members/" + url
        try:
            r = requests.get(url, cookies=cookies, headers=headers)
        except:
            print("Connection Error, Skipping Scene")
            return 1
        soup = BeautifulSoup(r.content, "html5lib")
        title = soup.find("title").text
        if title == "Error 401":
            print("Error 401")
            exit(1)
        # for link in soup.find_all("a", href=True):
        # if any(x in link['href'] for x in ['', '', '']):

    elif site == "pervcity":
        try:
            r = requests.get(url, cookies=cookies, headers=headers)
        except:
            print("Connection Error, Skipping Scene")
            return 1
        soup = BeautifulSoup(r.content, "html5lib")
        title = soup.find("title").text
        if title == "New Device Activation":
            print("New Device or Location Detected!", "\n")
            return 1
        data = soup.find_all("a", {"href": "javascript:void(0)"})
        for i in range(0, 12):
            if quality in str(data[i]).split('"')[3]:
                a = 1
                url = "https://members.pervcity.com/" + str(data[i]).split('"')[3]
                break
            elif (
                quality == "2160p"
                and "mp412000" in str(data[i]).split('"')[3]
                and a != 1
            ):
                url = "https://members.pervcity.com/" + str(data[i]).split('"')[3]
                break
        if want == "download":
            download(url)
        elif want == "print":
            print(url, "\n")
        elif want == "fprint":
            print(get_filename(url), " copied!")
            print(url, file=f)


def getmodelnum(model):
    s = []
    if site == "bangbros":
        model = model.split(" , ")
        for models in model:
            url = "https://members.bluepillmen.com/search?models=" + models.replace(
                " ", "+"
            )
            r = requests.get(url, cookies=cookies, headers=headers)
            soup = BeautifulSoup(r.content, "html5lib")
            s.append(
                int(
                    soup.find_all("div", class_="vdo")[0]
                    .text.splitlines()[1]
                    .split(' ""')[0]
                )
            )
        return s


def getmodels(model):
    a = []
    if (
        site == "analonly"
        or site == "trueanal"
        or site == "allanal"
        or site == "nympho"
        or site == "swallowed"
    ):

        url = "https://members." + site + ".com/models/" + model.replace(" ", "-")
        r = requests.get(url, cookies=cookies, headers=headers)
        if r.status_code == 500:
            print("Invalid Model Name")
            exit(1)
        soup = BeautifulSoup(r.content, "html5lib")
        s = []
        for link in soup.find_all("a", href=True):
            if "/scenes/" in link["href"] and link["href"] != a:
                a = link["href"]
                s.append(link["href"])
        if site == "trueanal":
            s = s[:-2]
        for i in range(len(s)):
            downloadlink(s[i])

    elif site == "bangbros":
        url = "https://members.bluepillmen.com/search?models=" + model.replace(
            " , ", "|"
        ).replace(" ", "+")
        r = requests.get(url, cookies=cookies, headers=headers)
        soup = BeautifulSoup(r.content, "html5lib")
        s = int(
            soup.find_all("div", class_="vdo")[0].text.splitlines()[1].split(' ""')[0]
        )
        if s >= 10437 or s == 0 or ("," in model and s in getmodelnum(model)):
            print("Invalid Model Name")
            exit(1)
        a = []
        for link in soup.find_all("a", href=True):
            if "/movie/" in link["href"] and link["href"] != a:
                a = link["href"]
                downloadlink(link["href"])

    elif site == "pervcity":
        url = (
            "https://members.pervcity.com/search.php?st=advanced&qall="
            + model.replace(",", "%2C").replace(" ", "+")
        )
        r = requests.get(url, cookies=cookies, headers=headers)
        if r.text == "Page not found":
            print("Invalid Model Name")
            exit(1)
        soup = BeautifulSoup(r.content, "html5lib")
        for link in soup.find_all("a", href=True):
            if "/scenes/" in link["href"] and link["href"] != a:
                a = link["href"]
                downloadlink(link["href"])


def getlinks(i):
    if (
        site == "analonly"
        or site == "trueanal"
        or site == "allanal"
        or site == "nympho"
        or site == "swallowed"
    ):
        url = "https://members." + site + ".com/scenes?page=" + i
        key = "/scenes/"
    if site == "bangbros":
        url = "https://members.bluepillmen.com/product/1/videos/latest/" + i
        key = "/movie/"
    elif site == "newsensations":
        # url = "https://newsensations.com/members/" + i
        # key = "gallery"
        print("need passes!")
        exit(1)
    elif site == "pervcity":
        url = "https://members.pervcity.com/categories/movies_" + i + "_d.html"
        key = "/scenes/"
    try:
        r = requests.get(url, cookies=cookies, headers=headers)
    except:
        print("Connection Error, Skipping Page")
        return 1
    soup = BeautifulSoup(r.content, "html5lib")
    a = []
    for link in soup.find_all("a", href=True):
        if key in link["href"] and link["href"] != a:
            a = link["href"]
            downloadlink(link["href"])
            time.sleep(0.001)


x = 1
y = 1

if site != "newsensations" and method != "models":
    x = int(input("Enter Starting Page: "))
    y = int(input("Enter Ending Page: "))
    while True:
        if x > y:
            print("Invalid Number, Enter Again")
        else:
            if x == 0:
                x += 1
            y += 1
            break

if method == "pages":
    for i in range(x, y):
        getlinks(str(i))

if method == "models":
    getmodels(model)

if want == "fprint":
    f.close()
